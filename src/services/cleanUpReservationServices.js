const connect = require("../db/connect");

async function cleanUpReservation(){
    const currentDate = new Date();

    //Definir a data para 7 dias atrás
    currentDate.setDate(currentDate.getDate() - 14) 
    //Formata a data para YYYY-MM-DD
    const formattedDate = currentDate.toISOString().split('T')[0];

    const query = `DELETE FROM schedule WHERE date < ?`
    const values = [formattedDate];

    return new Promise((resolve, reject) => {
        connect.query(query, values, function(err, results){
            if(err){
                console.error("Erro MySql", err);
                return reject(new Error("Erro ao limpar reservas"));
            }
            console.log("Reservas antigas apagadas");
            resolve("Reservas limpas com sucesso");
        })
    })
}

module.exports = cleanUpReservation;