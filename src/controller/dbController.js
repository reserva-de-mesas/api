const connect = require("../db/connect");

class dbController {

    // Método para obter os nomes das tabelas do banco de dados
    static async getTableNames(req, res) {
        // Query para mostrar as tabelas
        const queryShowTables = "SHOW TABLES";

        // Executar a consulta ao banco de dados
        connect.query(queryShowTables, async function (err, result) {
            if (err) {
                // Lidar com erros, se houver
                console.log(err); // Registrar o erro no console
                return res.status(500).json({ error: "Erro ao obter tabelas do banco de dados" }); // Responder com status de erro em JSON
            }

            // Extrair os nomes das tabelas dos resultados da consulta
            const tableNames = result.map(row => Object.values(row)[0]);
            // Responder com os nomes das tabelas em JSON
            res.status(200).json({ tables: tableNames });
        });
    }

    // Método para realizar o login
    static async login(req, res) {
        // Extrair CPF do corpo da requisição
        const { cpf } = req.body;

        // Verificar se o CPF foi fornecido
        if (cpf) {
            console.log(cpf); // Registrar o CPF no console
            // Responder com mensagem de sucesso em JSON
            return res.status(200).json({ message: "Login feito com sucesso!" });
        } else {
            // Caso contrário, informar que campos estão faltando
            return res.status(400).json({ message: "Por favor, preencha todos os campos." });
        }
    }

    // Método para obter descrições das tabelas do banco de dados
    static async getTableDescriptions(req, res) {
        // Query para mostrar as tabelas
        const queryShowTables = "SHOW TABLES";

        // Executar a consulta ao banco de dados
        connect.query(queryShowTables, async function (err, result) {
            if (err) {
                // Lidar com erros, se houver
                console.log(err); // Registrar o erro no console
                return res.status(500).json({ error: "Erro ao obter tabelas do banco de dados" }); // Responder com status de erro em JSON
            }

            const tables = [];
            // Iterar sobre os resultados para obter descrição de cada tabela
            for (let row of result) {
                const tableName = Object.values(row)[0];
                const queryDescTable = `DESCRIBE ${tableName}`;

                try {
                    // Obter a descrição da tabela usando uma promessa
                    const tableDescription = await new Promise((resolve, reject) => {
                        connect.query(queryDescTable, function (err, result) {
                            if (err) {
                                reject(err);
                            } else {
                                resolve(result);
                            }
                        });
                    });

                    // Armazenar nome e descrição da tabela no array de tabelas
                    tables.push({ name: tableName, description: tableDescription });
                } catch (error) {
                    // Lidar com erros ao obter descrição da tabela
                    console.log(error); // Registrar o erro no console
                    return res.status(500).json({ error: `Erro ao obter a descrição da tabela ${tableName}` }); // Responder com status de erro em JSON
                }
            }

            // Responder com as descrições das tabelas em JSON
            res.status(200).json({ tables });
        });
    }
}

module.exports = dbController;
