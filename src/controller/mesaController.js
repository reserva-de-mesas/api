// Importa o módulo de conexão com o banco de dados e o configura para usar Promessas
const connect = require("../db/connect").promise();

// Exporta a classe MesaController para ser usada em outros arquivos
module.exports = class MesaController {

    // Método assíncrono para criar uma nova mesa
    static async criarMesa(req, res) {
        // Extrai capacidade e status do corpo da requisição
        const { capacidade, status } = req.body;

        // Verifica se capacidade e status estão presentes e se o status é válido
        if (capacidade && (status === "1" || status === "0")) {
            // Prepara a consulta SQL de inserção
            const query = `INSERT INTO mesa (capacidade, status) VALUES (?, ?)`;
            const values = [capacidade, status]; // Converte o status para 1 ou 0

            try {
                // Executa a consulta SQL e espera o resultado
                await connect.query(query, values);
                console.log("Mesa cadastrada com sucesso!"); // Log de sucesso
                res.status(200).json({ message: "Mesa cadastrada com sucesso!" }); // Responde com mensagem de sucesso
            } catch (error) {
                // Em caso de erro, loga e responde com mensagem de erro
                console.error("Erro ao cadastrar mesa:", error);
                res.status(500).json({ error: "Erro ao cadastrar mesa no banco de dados" });
            }
        } else {
            // Se capacidade ou status não forem fornecidos ou status for inválido, responde com mensagem de erro
            res.status(400).json({ message: "Por favor, preencha todos os campos corretamente." });
        }
    }

    // Método assíncrono para editar uma mesa existente
    static async editMesa(req, res) {
        // Extrai capacidade, status e ID da mesa do corpo e dos parâmetros da requisição
        const { capacidade, status } = req.body;
        const mesaId = req.params.id; // Alteração aqui para usar um nome mais descritivo
    
        try {
            // Verifica se o status é válido
            if (status === "0" || status === "1") {
                // Prepara a consulta SQL de atualização
                const query = `UPDATE mesa SET capacidade = ?, status = ? WHERE id_mesa = ?`;
                const values = [capacidade, status, mesaId];
    
                // Executa a consulta SQL e espera o resultado
                await connect.query(query, values);
                console.log("Mesa editada com sucesso"); // Log de sucesso
                res.status(200).json({ message: "Mesa editada com sucesso" }); // Responde com mensagem de sucesso
            } else {
                // Se o status for inválido, responde com mensagem de erro
                res.status(400).json({ message: "Por favor, preencha todos os campos corretamente." });
            }
        } catch (error) {
            // Em caso de erro, loga e responde com mensagem de erro
            console.error("Erro ao editar mesa:", error);
            res.status(500).json({ error: "Erro ao editar mesa no banco de dados" });
        }
    }
    
    // Método assíncrono para obter informações de todas as mesas
    static async getMesa(req, res) {
        try {
            // Prepara a consulta SQL de seleção
            const query = `SELECT * FROM mesa`;

            // Executa a consulta SQL e espera o resultado
            const [rows, fields] = await connect.query(query);
            console.log("Mesas obtidas com sucesso"); // Log de sucesso
            res.status(200).json(rows); // Responde com os dados obtidos
        } catch (error) {
            // Em caso de erro, loga e responde com mensagem de erro
            console.error("Erro ao obter mesas:", error);
            res.status(500).json({ error: "Erro ao obter mesa do banco de dados" });
        }
    }

    // Método assíncrono para excluir uma mesa existente
    static async deletarMesa(req, res) {
        try {
            // Verifica se o ID da mesa foi fornecido
            if (!req.params.id) {
                // Se não foi fornecido, responde com mensagem de erro
                return res.status(400).json({ message: "ID da mesa não fornecido" });
            }

            // Extrai o ID da mesa dos parâmetros da requisição
            const id = req.params.id;

            // Prepara a consulta SQL de exclusão
            const query = "DELETE FROM mesa WHERE id_mesa = ?";

            // Executa a consulta SQL e espera o resultado
            const result = await connect.query(query, [id]);

            // Verifica se a mesa foi excluída com sucesso e responde de acordo
            if (result[0].affectedRows === 0) {
                console.log("Nenhuma mesa foi excluída"); // Log de falha
                return res.status(404).json({ message: "Mesa não encontrada" }); // Responde com mensagem de erro
            }

            console.log("Mesa excluída com sucesso"); // Log de sucesso
            res.status(200).json({ message: "Mesa excluída" }); // Responde com mensagem de sucesso
        } catch (error) {
            // Em caso de erro, loga e responde com mensagem de erro
            console.error("Erro interno ao excluir mesa:", error);
            res.status(500).json({ error: "Erro interno do servidor" });
        }
    }
};
