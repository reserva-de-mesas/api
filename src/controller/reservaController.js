const connect = require("../db/connect");

module.exports = class ReservaController {
  static async criarReserva(req, res) {
    const { date, timeStart, timeEnd, cliente, mesa } = req.body;

    if (!date || !timeStart || !timeEnd || !cliente || !mesa) {
      return res
        .status(400)
        .json({ error: "todos os campos devem ser preenchidos" });
    }

    try {
      // Verificar se já existe uma reserva
      const overlapQuery = `SELECT * FROM reserva WHERE id_mesa = ${mesa} AND date = '${date}' AND ((timeStart <= '${timeStart}' AND timeEnd > '${timeStart}') OR (timeStart < '${timeEnd}' AND timeEnd >= '${timeEnd}'))`;

      connect.query(overlapQuery, function (err, results) {
        if (err) {
          console.log(err);
          return res
            .status(400)
            .json({ error: "Erro ao verificar reserva existente" });
        }
        if (results.length > 0) {
          return res
            .status(400)
            .json({ error: "Mesa já reservada para este dia e horário" });
        }

        // Inserir reserva na tabela
        const insertQuery = `INSERT INTO reserva (date, timeStart, timeEnd, cpf, id_mesa) VALUES ('${date}', '${timeStart}', '${timeEnd}', '${cliente}', ${mesa})`;

        connect.query(insertQuery, function (err, results) {
          if (err) {
            console.error(err);
            return res.status(400).json({ error: "Erro ao cadastrar reserva" });
          }
          return res
            .status(201)
            .json({ message: "Reserva realizada com sucesso" });
        });
      }); // Fim função de overlapQuery
    } catch (error) {
      console.error("Erro ao exucutar a consulta:", error);
      res.status(500).json({ error: "Erro interno do servidor" });
    }
  } // Fim da função criarReserva

  static async obterReservas(req, res) {
    try {
      const query = `SELECT * FROM reserva`;
      connect.query(query, function (err, result) {
        if (err) {
          console.log(err);
          return res.status(500).json({ error: "Erro ao buscar reservas" });
        }
        console.log("Reservas obtidas com sucesso");
        return res.status(200).json(result);
      });
    } catch (error) {
      console.error("Erro ao buscar reservas:", error);
      return res.status(500).json({ error: "Erro interno do servidor" });
    }
  }
};
