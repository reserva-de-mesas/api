const connect = require("../db/connect");

module.exports = class ClienteController {
    static async criarCliente(req, res) {
        const { nome, email, telefone, cpf, senha, confirmarSenha } = req.body;

        if (!nome || !email || !telefone || !cpf || !senha || !confirmarSenha) {
            return res.status(400).json({ message: "Por favor, preencha todos os campos." });
        }

        if (!email.includes("@")) {
            return res.status(400).json({ message: "Por favor, insira um email válido" });
        }

        const cpfRegex = /^\d{11}$/;
        const telefoneRegex = /^\d{11}$/;

        if (!cpfRegex.test(cpf)) {
            return res.status(400).json({ message: "CPF incompleto" });
        }

        if (!telefoneRegex.test(telefone)) {
            return res.status(400).json({ message: "Telefone incompleto" });
        }


        try {
            const emailUnique = await new Promise((resolve, reject) => {
                const query = `SELECT COUNT(*) as count FROM cliente WHERE email = '${email}'`;
                connect.query(query, (err, result) => {
                    if (err) return reject(err);
                    resolve(result[0].count === 0);
                });
            });

            if (!emailUnique) {
                return res.status(400).json({ error: "Email já cadastrado" });
            }

            const telefoneUnique = await new Promise((resolve, reject) => {
                const query = `SELECT COUNT(*) as count FROM cliente WHERE telefone = '${telefone}'`;
                connect.query(query, (err, result) => {
                    if (err) return reject(err);
                    resolve(result[0].count === 0);
                });
            });

            if (!telefoneUnique) {
                return res.status(400).json({ error: "Telefone já cadastrado" });
            }

            const cpfUnique = await new Promise((resolve, reject) => {
                const query = `SELECT COUNT(*) as count FROM cliente WHERE cpf = '${cpf}'`;
                connect.query(query, (err, result) => {
                    if (err) return reject(err);
                    resolve(result[0].count === 0);
                });
            });

            if (!cpfUnique) {
                return res.status(400).json({ error: "CPF já cadastrado" });
            }

            if (senha !== confirmarSenha) {
                return res.status(400).json({ message: "As senhas informadas não coincidem" });
            }

            const query = `INSERT INTO cliente (nome, email, telefone, cpf, senha) VALUES ('${nome}', '${email}', '${telefone}', '${cpf}', '${senha}')`;

            connect.query(query, (err) => {
                if (err) {
                    console.log(err);
                    return res.status(500).json({ error: "Erro ao cadastrar cliente no banco de dados" });
                }
                console.log("Cliente cadastrado com sucesso!");
                return res.status(200).json({ message: "Cadastrado com sucesso!" });
            });

        } catch (error) {
            console.error("Erro ao registrar cliente:", error);
            return res.status(500).json({ error: "Erro interno do servidor" });
        }
    }



    static async autenticarCliente(req, res) {
        const { cpf, senha } = req.body;

        if (cpf && senha) {
            const query = `SELECT * FROM cliente WHERE cpf = '${cpf}'`;

            try {
                connect.query(query, function (err, results) {
                    if (err) {
                        console.error("Erro ao autenticar cliente:", err);
                        return res.status(500).json({ error: "Erro ao autenticar cliente" });
                    }

                    if (results.length > 0) {
                        const cliente = results[0];

                        if (cliente.senha === senha) {
                            return res.status(200).json({ message: "Autenticado com sucesso!", cliente: cliente });
                        } else {
                            return res.status(401).json({ error: "Credenciais inválidas" });
                        }
                    } else {
                        return res.status(404).json({ error: "Cliente não encontrado" });
                    }
                });
            } catch (error) {
                console.error("Erro ao autenticar cliente:", error);
                return res.status(500).json({ error: "Erro interno do servidor" });
            }
        } else {
            return res.status(400).json({ message: "Por favor, insira cpf e senha" });
        }
    }

    static async getCliente(req, res) {
        try {
            const query = `SELECT * FROM cliente`;
            connect.query(query, function (err, result) {
                if (err) {
                    console.log(err);
                    return res.status(500).json({ error: "Erro ao obter cliente do banco de dados" });
                }
                console.log("Cliente obtido com sucesso");
                return res.status(200).json(result);
            });
        } catch (error) {
            console.error("Erro ao obter cliente:", error);
            return res.status(500).json({ error: "Erro interno do servidor" });
        }
    }
};
