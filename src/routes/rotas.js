const router = require("express").Router();
const dbController = require("../controller/dbController");
const ClienteController = require("../controller/clienteController");
const MesaController = require("../controller/mesaController");
const ReservaController = require("../controller/reservaController")

router.get("/tables", dbController.getTableNames);
router.get("/tables/descriptions", dbController.getTableDescriptions);

router.post("/cadastro", ClienteController.criarCliente);
router.post("/login",ClienteController.autenticarCliente);
router.get("/clientes", ClienteController.getCliente);

router.get("/mesa", MesaController.getMesa)
router.post("/cadastroMesa", MesaController.criarMesa);
router.put("/editarMesa/:id", MesaController.editMesa);
router.delete("/deletarMesa/:id", MesaController.deletarMesa);

router.post("/criarReserva", ReservaController.criarReserva);
router.get("/reservas", ReservaController.obterReservas);


module.exports = router;
